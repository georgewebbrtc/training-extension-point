<?php

require_once "classes/MonitoringBase.class.php";

class Monitoring extends MonitoringBase
{
    protected function checkAdditional(&$result)
    {
        if ($this->shouldCheck("random-check")) {
            $result["random-check"] = $this->checkRandom();
        }
    }

    protected function checkRandom()
    {
        return rand(0, 10) % 2 === 0;
    }
}
