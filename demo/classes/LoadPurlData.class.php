<?php

require_once "classes/LoadPurlDataBase.class.php";

class LoadPurlData extends LoadPurlDataBase
{
    public static function format($data, $request = array(), $is_preview = false)
    {
        $data['customer_name_uc'] = strtoupper($data['customer_name']);
        return $data;
    }
}
