<?php

class ValidationController
{
    public function verifyAge()
    {
        $age = empty($_GET['age']) ? false : (int) $_GET['age'];
        if (!$age) {
            echo json_encode(array(
                'status' => 'failed',
                'message' => 'Age was not valid'
            ));
            return;
        }

        if ($age < 18) {
            echo json_encode(array(
                'status' => 'failed',
                'message' => 'Too young!'
            ));
            return;
        }

        echo json_encode(array(
            'status' => 'success',
            'message' => ''
        ));
    }
}
