<?php

require_once "classes/ValidationController.php";

R("verify_age")
    ->controller("ValidationController")
    ->action("verifyAge")
    ->on("POST");
