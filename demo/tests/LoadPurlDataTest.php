<?php

require "classes/LoadPurlData.class.php";
require_once "tests/php/EovTestCase.php";

class LoadPurlDataTest extends EovTestCase
{
    public function testFormat()
    {
        $cases = array(
            array(
                'input' => array(
                    'customer_name' => 'george',
                    'agent_name' => 'john',
                    'agent_id' => '123'
                ),
                'expected' => array(
                    'customer_name' => 'george',
                    'agent_name' => 'john',
                    'agent_id' => '123',
                    'customer_name_uc' => 'GEORGE'
                )
            ),
            array(
                'input' => array(
                    'customer_name' => 'Karl',
                    'agent_name' => 'john',
                    'agent_id' => '123'
                ),
                'expected' => array(
                    'customer_name' => 'Karl',
                    'agent_name' => 'john',
                    'agent_id' => '123',
                    'customer_name_uc' => 'KARL'
                )
            )
        );

        foreach ($cases as $case) {
            $actual = LoadPurlData::format($case['input']);
            $this->assertEquals($case['expected'], $actual);
        }
    }
}
